from labtools.table.cell import Cell, EmptyCell

class Column:
    def __init__(self, cells, alignment, header=None, footer=None, columns=None):
        if columns is None:
            columns = len(alignment.split(" "))
        self._columns = columns
        self._cells = []
        for c in cells:
            self.add_cell(c)
        if header is None:
            header = EmptyCell(columns)
        if not isinstance(header, Cell):
            header = Cell(header, columns=columns)
        if footer is None:
            footer = EmptyCell(columns)
        if not isinstance(footer, Cell):
            footer = Cell(footer, columns=columns)
        self._alignment = alignment
        self._header = header
        self._footer = footer

    def cell(self, row):
        if row < 0 or row >= len(self._cells):
            return EmptyCell(self._columns)
        return self._cells[row]

    def alignment(self):
        return self._alignment

    def header(self):
        return self._header

    def footer(self):
        return self._footer

    def add_cell(self, cell):
        if not isinstance(cell, Cell):
            cell = Cell(cell)
        cell.set_columns(self._columns)
        self._cells.append(cell)

class DummyColumn(Column):
    def __init__(self, alignment, columns=0):
        Column.__init__(self, [], alignment, columns=columns)

class VerticalRule(DummyColumn):
    def __init__(self, rules=1):
        DummyColumn.__init__(self, "|" * rules)

class VerticalSpace(Column):
    def __init__(self, length=1, space='0em'):
        Column.__init__(self, [[r'\hspace{{{:s}}}'.format(space)] * int(length)], 'c', header=r'\hspace{{{:s}}}'.format(space))
