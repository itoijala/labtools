from decimal import Decimal
from math import floor, log10
import re

from labtools.common import heaviside

def block_join(blocks, separator=""):
    lists = [ s.split("\n") for s in blocks ]
    lens = [ max([ len(s) for s in l ]) for l in lists ]
    lists = [ [ s + (" " * heaviside(le - len(s))) for s in l ] for (l, le) in zip(lists, lens) ]
    m = max([ len(l) for l in lists ])
    lists = [ l + ([" " * le] * heaviside(m - len(l))) for (l, le) in zip(lists, lens) ]
    strs = []
    for i in range(len(lists[0])):
        strs.append(separator.join([ l[i] for l in lists ]))
    return "\n".join(strs)

def empty_cell(columns):
    return " ".join(["&"] * heaviside(columns - 1))

def table_format(values):
    if len(values) == 0:
        return "0.0"
    if isinstance(values, str):
        values = [values]

    # Only valid float strings need to be considered:
    values = [ v for v in values if not v in ['NaN', 'nan', ''] ]
    groups = [re.match(r"(\+|-)?([0-9]*)(?:\.([0-9]+))?(?:e(\+|-)?([0-9]+))?$", v).groups() for v in values ]

    s = "-" if max([ 0 if g[0] is None else 1 for g in groups ]) > 0 else ""
    i = max([ 0 if g[1] is None else len(g[1]) for g in groups ])
    f = max([ 0 if g[2] is None else len(g[2]) for g in groups ])
    es = "-" if max([ 0 if g[3] is None else 1 for g in groups ]) > 0 else ""
    e = max([ 0 if g[4] is None else len(g[4]) for g in groups ])

    return "{:s}{:d}.{:d}{:s}".format(s, i, f, "e{:s}{:d}".format(es, e) if e > 0 else "")
