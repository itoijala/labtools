import numpy
from matplotlib.ticker import Formatter

from labtools.common import round_places

class SiFormatter(Formatter):
    def __init__(self, places=0, factor=1, exp=False, offset=0):
        self.places = places
        self.factor = factor
        self.exp = exp
        self.offset = offset

    def format_data(self, value, pos=None):
        value += self.offset
        value *= self.factor
        if not self.exp:
            return r"\num{{{:s}}}".format(round_places(value, self.places))
        else:
            return r"\num{{{:.{:d}e}}}".format(value, self.places)

    __call__ = format_data

def ax_reverse_legend(ax, loc, **kwargs):
    handles, labels = ax.get_legend_handles_labels()
    ax.legend(handles[::-1], labels[::-1], loc=loc, **kwargs)

def legend_order(ax, order, **kwargs):
    handles, labels = numpy.array(ax.get_legend_handles_labels())
    ax.legend(list(handles[order]), list(labels[order]), **kwargs)
